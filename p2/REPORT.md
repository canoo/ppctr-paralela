# P2: MatMul

­Practica 2
Programación paralela
Alejandro Cano Cos.


Ejercicio 1

a) #pragma omp parallel private(i,j,k) shared(A, B, C, dim) num_threads(4)

Esto indica que el hilo que lee esto crea una region paralela y se convierte en el hilo master. Las instrucciones que esten incluidas en la region seran ejecutadas por todos los hilos, y cada hilo tendra sus porpias variables i,j,k al haberlas declarado privadas, y A,B,C, dim seran públicas y todos tendran acceso a estas direcciones de memoria.
El num_threads(4) nos indica que en la region paralela debemos usar 4 threads. Si no lo usamos, se usará la función del runtime_omp_set_numthreads().


#pragma omp for

Directiva que indica a los hilos de la region paralela que lo que viene a continuacion es un for, y se ejecutaran en paralelo las iteraciones.

#pragma omp for nowait

Directiva igual que la anterior, solo que cuando acabe el for, los hilos continuaran.

b) private(i,j,k) shared(A, B, C, dim)

I,j,k las pongo privadas porque cada thread llevara la cuenta de su “puntero” en el paralelismo de los bucles for, y son compartidas A,B,C, dim porque son las matrices que se van a usar y se van a acceder a sus datos, y escribirlos de manera global.

c) En las secciones paralelas, todos los threads ejecutan las mismas instrucciones, solo que cuando llegas a alguna directiva, en este caso la del for, los threads se dividen el trabajo. Si no planificas la ejecucion de los threads (para dividir la carga de trabajo de una determinada manrera) , con la directiva schedule, se toma la decision en tiempo de ejecucion, de la variable de entorno OMP_SCHEDULE. Habria 3 tipos de planificacion en este caso: static, dynamic y guided.

d) Multiplicando matrices 1000 x 1000 con 1 unico thread: 6 segs.
Multiplicando matrices 1000 x 1000 con 4 threads: 2 segs.
Es un 300% mejor multiplicar con 4 threads que con 1. He escogido 4 threads porque mi ordenador tiene 4 cores, por lo que es la manera mas eficiente de paralelizar las instrucciones.


Ejercicio 2

a) Sin utilizar schedules mat_mul es mas lento(24 segs vs 16-17 segs), esto se debe a que el metodo mat_mul_sup multiplica solamente los elementos de la diagonal superior, por lo que el metodo tiene menor complejidad y duracion, asi que en general va a acabar antes.

b)

Utilizo matrices de 2000 x 2000
Sin paralelizar: 28 segs
Sin schedule paralelizado: 16 segundos.
Con schedule static 17 segs
Con shedule dinamico: 10 segs
Con schedule guided: 18 segs

Asi que el dinamico obtiene mejor tiempo, pero con mucha diferencia, ya que el trabajo a cada thread se le asigna cuando este libre. Debo añadir que estos tiempos han sido tomados sin establecer el tamaño de bloque,
pero con el schedule(static,1), se puede obtener un tiempo minimo de ~8 s, al cual los demas no pueden bajar.

El estatico divide las iteraciones de forma igual a todos.
El guiado hace la carga mas pequeña conforme nos acercamos al final, para regular el trabajo.
El dinamico reparte trabajo entre los threads que esten libres en todo momento.

c) Para el static el bloque optimo seria de 1, ya que esto reparte mejor la carga de trabajo, la cual disminuye de manera lineal a medida que aumentamos las iteraciones.
Ademas, la combinacion schedule(static, 1), es la combinacion mas rapida, ya que no habria casi diferencia en la carga de trabajo de todos y no habria overhead por el shedule.

d)

750 ---- 22.674922 s
500 ---- 17.718448 s
400 ---- 14.868709 s
300 ---- 11.531024 s
200 ---- 9.985643 s
150 ---- 9.836956 s
100 ---- 9.738834 s
50 ----  9.662828 s
5 ---- 9.704395 s
1 ---- 9.765565 s



Segun la tabla, con 50 iteraciones seria el tiempo minimo alcanzado por el programa.

Con 50 iteraciones, comparamos el primer bloque con el siguiente:

50 x dim x (dim + dim-1….dim-49) → Primer bloque
50 x dim x (dim-50 + dim -51… dim -99) → Segundo bloque

Para calcular el % de diferencia, dividimos el primero entre el segundo …:

 (50*dim + (0 -1 -2 -3 -4 -5….-49)) / (50*dim + (-50 -52 -53… -99)) =
 (50*2000-1225) / (50*2000- 3725) = 98775 /96275 = 1.025

Con 200 iteraciones, comparamos el primer bloque con el siguiente:

200 x dim x (dim + dim-1….dim-199) → Primer bloque
200 x dim x (dim-200 + dim -51… dim -499) → Segundo bloque

Para calcular el % de diferencia, dividimos el primero entre el segundo …:

 (200*dim + (0 -1 -2 -3 -4 -5….-199)) / (200*dim + (-200 -201 -202… -499)) =
 (200*2000-19900) / (200*2000- 104850) = 380100 /  295150 = 1.28


Con 500 iteraciones, comparamos el primer bloque con el siguiente:

500 x dim x (dim + dim-1….dim-499) → Primer bloque
500 x dim x (dim-500 + dim -501… dim -999) → Segundo bloque

Para calcular el % de diferencia, dividimos el primero entre el segundo …:

 (500*dim + (0 -1 -2 -3 -4 -5….-499)) / (500*dim + (-501 -502 -503… -99)) =
 (500*2000- 124750) / (500*2000- 374750 ) = 875250 / 625250 = 1.40

Comparamos los resultados obtenidos:

Con bloques de 50, obtenemos una carga de trabajo respecto al siguiente bloque de x1.025, lo cual es una carga casi inapreciable, pasaria mas o menos desapercibida, pero con tamaños de bloque de 500 la carga se hace mucho mas notable, ya que es x1.40, es decir, tienes el mismo trabajo del siguiente bloque, sumandole una mitad mas del trabajo de este, Obviamente esto produce un desequilibrio bastante grande, que se ve reflejado en la tabla de tiempos de arriba.

Tambien, tenemos que de 50 a 200 iteraciones hay bastante diferencia entre los desvios de trabajo, pero los tiempos son parecidos, esto sucede, por que la planificacion dinamica tiene “overhead”, podriamos decir que el % de cpu util es peor que el estatico, porque hay que hacer mas gestiones de planificacion para asignar un trabajo a cada thread, por lo que si ponemos un tamaño de bloque pequeño, habra que hacer mas gestiones de planificacion en tiempo de ejecucion.

En resumen, lo mejor es un tamaño de bloque ~50, ya que sino se nota la diferencia de trabajo.

e)El guided tarda ~16 segs en ejecutarse, este lo que hace es empezar cogiendo tamaños de bloque mas grandes y hacerles cada vez mas pequeños.
Tiene mas overhead que los demas y para este caso no es muy recomendable.


Ejercicio 3

a) El metodo recorre el mismo numero de elementos que mul_sup, solo que  que recorre la mitad derecha en vez de la izquierda, y la carga de trabajo es ascendente

b) Ahora el algoritmo guided es muy eficiente, dado que la carga de trabajo es ascendente y el tamaño de bloque descendente se consigue repartir el trabajo adecuadamente.

c) El peor en mi opinion seria el static (si no asignas tamaño de bloque), si lo asignas el peor es el dynamic, ya que schedule(static,1) distribuye el trabajo de mejor manera y tiene menos overhead
que dynamic.
