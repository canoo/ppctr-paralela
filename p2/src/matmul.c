#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>
#include <omp.h>


#define RAND rand() % 100

void init_mat_sup ();
void init_mat_inf ();
void matmul ();
void matmul_sup ();
void matmul_inf ();
void print_matrix (float *M, int dim); // TODO

/* Usage: ./matmul <dim> [block_size]*/

int main (int argc, char* argv[])
{
	int block_size = 1, dim=0;
	float *A, *B, *C;
	int i;
	int j;

	if(argc <3){
		printf("Mete argumentos \n");
		return 1;}

	dim = atoi (argv[1]);
	if (argc == 3) block_size = atoi (argv[2]);

	A = (float *) malloc( dim * dim * sizeof(float) );
	B = (float *) malloc( dim * dim * sizeof(float) );
	C = (float *) malloc( dim * dim * sizeof(float) );

	FILE *f = fopen("matrix.in", "r");

	if(f==NULL){
		printf("No se ha podido abrir el fichero.\n");
		return 0;
	}

	int p;
	fscanf(f,"%d",&p);
	fscanf(f,"%d",&p);

	for(i=0; i<dim; i++){
		for (j = 0; j < dim; j++) {
			fscanf(f, "%f" ,&A[i*dim +j]);
			//printf("%f ", (A[i*dim +j]));
		}
		//printf("\n");
	}
	//printf("\n \n");

	fscanf(f,"%d",&p);
	fscanf(f,"%d",&p);

	for(i=0; i<dim; i++){
		for (j = 0; j < dim; j++) {
			fscanf(f, "%f" ,&B[i*dim +j]);
			//printf("%f ", B[i*dim +j]);
		}
		//printf("\n");
	}

	fclose(f);

	//printf("\n \n");

	double ini,fin;


	ini = omp_get_wtime();


	//matmul(A,B,C,block_size); //Ej 1
	matmul_sup(A,B,C,block_size);
	//matmul_sup(dim,C);

	fin = omp_get_wtime();

/**	for(i=0; i<dim; i++){
	for (j = 0; j < dim; j++) {
	printf("%f ",C[i*dim +j]);
}
printf("\n");
}*/

printf("\n \n");
// printf("Holaaaaaaaaa");
printf(" %lf %lf El tiempo empleado: %lf \n",ini,fin,fin-ini );

exit (0);
}

void init_mat_sup (int dim, float *M)
{
	int i,j,m,n,k;

	#pragma omp parallel private(i,j) shared(M, dim) num_threads(4)
	{
		#pragma omp for nowait schedule(dynamic,750)
		for (i = 0; i < dim; i++) {
			for (j = 0; j < dim; j++) {
				if (j <= i)
				M[i*dim+j] = 0.0;
				else
				M[i*dim+j] = RAND;
			}
		}
	}
}

void init_mat_inf (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j >= i)
			M[i*dim+j] = 0.0;
			else
			M[i*dim+j] = RAND;
		}
	}
}

void matmul (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	#pragma omp parallel private(i,j,k) shared(A, B, C, dim) num_threads(4)
	{
		#pragma omp for
		for (i=0; i < dim; i++){

			for (j=0; j < dim; j++){
				C[i*dim+j] = 0.0;
			}

		}

		#pragma omp for nowait
		for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
		for (k=0; k < dim; k++)
		C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}

}

void matmul_sup (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	#pragma omp parallel private(i,j,k) shared(A, B, C, dim) num_threads(4)

	{
		#pragma omp for
		for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
		C[i*dim+j] = 0.0;

		#pragma omp for schedule(static,1) nowait
		for (i=0; i < (dim-1); i++)
		for (j=0; j < (dim-1); j++)
		for (k=0; k < i; k++)
		C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
}

void matmul_inf (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	C[i*dim+j] = 0.0;

	for (i=1; i < dim; i++)
	for (j=1; j < dim; j++)
	for (k=0; k < i; k++)
	C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}
