# Título de práctica

Autor Alejandro Cano

Fecha DD/MM/YYYY

## Prefacio

Objetivos conseguidos

He conseguido un speedup mayor al 2.5 en promedio que era mi objetivo.


## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.

- Ejecución sobre Máquina Virtual VirtualBox:
Arquitectura:          x86_64
modo(s) de operación de las CPUs:32-bit, 64-bit
Orden de bytes:        Little Endian
CPU(s):                4
On-line CPU(s) list:   0-3
Hilo(s) de procesamiento por núcleo:1
Núcleo(s) por «socket»:4
Socket(s):             1
Modo(s) NUMA:          1
ID de fabricante:      GenuineIntel
Familia de CPU:        6
Modelo:                94
Model name:            Intel(R) Core(TM) i7-6700 CPU @ 3.40GHz
Revisión:             3
CPU MHz:               3408.006
BogoMIPS:              6816.01
Fabricante del hipervisor:KVM
Tipo de virtualización:lleno
Caché L1d:            32K
Caché L1i:            32K
Caché L2:             256K
Caché L3:             8192K
NUMA node0 CPU(s):     0-3
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx rdtscp lm constant_tsc rep_good nopl xtopology nonstop_tsc pni pclmulqdq ssse3 cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx rdrand hypervisor lahf_lm abm 3dnowprefetch fsgsbase avx2 invpcid rdseed clflushopt
alex-virtualbox
    descripción: Project-Id-Version: lshwReport-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>POT-Creation-Date: 2009-10-08 14:02+0200PO-Revision-Date: 2012-03-14 06:38+0000Last-Translator: Paco Molinero <paco@byasl.com>Language-Team: Spanish <es@li.org>MIME-Version: 1.0Content-Type: text/plain; charset=UTF-8Content-Transfer-Encoding: 8bitX-Launchpad-Export-Date: 2016-06-27 17:08+0000X-Generator: Launchpad (build 18115)
    producto: VirtualBox
    fabricante: innotek GmbH
    versión: 1.2
    serie: 0
    anchura: 64 bits
    capacidades: smbios-2.5 dmi-2.5 vsyscall32
    configuración: family=Virtual Machine uuid=2BD49447-60F3-409F-B7F0-40CD9E1FB644
  *-core
       descripción: Placa base
       producto: VirtualBox
       fabricante: Oracle Corporation
       id físico: 0
       versión: 1.2
       serie: 0
     *-firmware
          descripción: BIOS
          fabricante: innotek GmbH
          id físico: 0
          versión: VirtualBox
          date: 12/01/2006
          tamaño: 128KiB
          capacidades: isa pci cdboot bootselect int9keyboard int10video acpi
     *-memory
          descripción: Memoria de sistema
          id físico: 1
          tamaño: 9852MiB
     *-cpu
          producto: Intel(R) Core(TM) i7-6700 CPU @ 3.40GHz
          fabricante: Intel Corp.
          id físico: 2
          información del bus: cpu@0
          anchura: 64 bits
          capacidades: fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx rdtscp x86-64 constant_tsc rep_good nopl xtopology nonstop_tsc pni pclmulqdq ssse3 cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx rdrand hypervisor lahf_lm abm 3dnowprefetch fsgsbase avx2 invpcid rdseed clflushopt
     *-pci
          descripción: Host bridge
          producto: 440FX - 82441FX PMC [Natoma]
          fabricante: Intel Corporation
          id físico: 100
          información del bus: pci@0000:00:00.0
          versión: 02
          anchura: 32 bits
          reloj: 33MHz
        *-isa
             descripción: ISA bridge
             producto: 82371SB PIIX3 ISA [Natoma/Triton II]
             fabricante: Intel Corporation
             id físico: 1
             información del bus: pci@0000:00:01.0
             versión: 00
             anchura: 32 bits
             reloj: 33MHz
             capacidades: isa bus_master
             configuración: latency=0
        *-ide
             descripción: IDE interface
             producto: 82371AB/EB/MB PIIX4 IDE
             fabricante: Intel Corporation
             id físico: 1.1
             información del bus: pci@0000:00:01.1
             versión: 01
             anchura: 32 bits
             reloj: 33MHz
             capacidades: ide bus_master
             configuración: driver=ata_piix latency=64
             recursos: irq:0 ioport:1f0(size=8) ioport:3f6 ioport:170(size=8) ioport:376 ioport:d000(size=16)
        *-display
             descripción: VGA compatible controller
             producto: VirtualBox Graphics Adapter
             fabricante: InnoTek Systemberatung GmbH
             id físico: 2
             información del bus: pci@0000:00:02.0
             versión: 00
             anchura: 32 bits
             reloj: 33MHz
             capacidades: vga_controller rom
             configuración: driver=vboxvideo latency=0
             recursos: irq:18 memoria:e0000000-e0ffffff memoria:c0000-dffff
        *-network
             descripción: Ethernet interface
             producto: 82540EM Gigabit Ethernet Controller
             fabricante: Intel Corporation
             id físico: 3
             información del bus: pci@0000:00:03.0
             nombre lógico: enp0s3
             versión: 02
             serie: 08:00:27:f1:4f:23
             tamaño: 1Gbit/s
             capacidad: 1Gbit/s
             anchura: 32 bits
             reloj: 66MHz
             capacidades: pm pcix bus_master cap_list ethernet physical tp 10bt 10bt-fd 100bt 100bt-fd 1000bt-fd autonegotiation
             configuración: autonegotiation=on broadcast=yes driver=e1000 driverversion=7.3.21-k8-NAPI duplex=full ip=10.0.2.15 latency=64 link=yes mingnt=255 multicast=yes port=twisted pair speed=1Gbit/s
             recursos: irq:19 memoria:f0000000-f001ffff ioport:d010(size=8)
        *-generic
             descripción: System peripheral
             producto: VirtualBox Guest Service
             fabricante: InnoTek Systemberatung GmbH
             id físico: 4
             información del bus: pci@0000:00:04.0
             versión: 00
             anchura: 32 bits
             reloj: 33MHz
             configuración: driver=vboxguest latency=0
             recursos: irq:20 ioport:d020(size=32) memoria:f0400000-f07fffff memoria:f0800000-f0803fff
        *-multimedia
             descripción: Multimedia audio controller
             producto: 82801AA AC'97 Audio Controller
             fabricante: Intel Corporation
             id físico: 5
             información del bus: pci@0000:00:05.0
             versión: 01
             anchura: 32 bits
             reloj: 33MHz
             capacidades: bus_master
             configuración: driver=snd_intel8x0 latency=64
             recursos: irq:21 ioport:d100(size=256) ioport:d200(size=64)
        *-usb
             descripción: USB controller
             producto: KeyLargo/Intrepid USB
             fabricante: Apple Inc.
             id físico: 6
             información del bus: pci@0000:00:06.0
             versión: 00
             anchura: 32 bits
             reloj: 33MHz
             capacidades: ohci bus_master cap_list
             configuración: driver=ohci-pci latency=64
             recursos: irq:22 memoria:f0804000-f0804fff
           *-usbhost
                producto: OHCI PCI host controller
                fabricante: Linux 4.10.0-28-generic ohci_hcd
                id físico: 1
                información del bus: usb@1
                nombre lógico: usb1
                versión: 4.10
                capacidades: usb-1.10
                configuración: driver=hub slots=12 speed=12Mbit/s
              *-usb
                   descripción: Dispositivo de interfaz humana
                   producto: USB Tablet
                   fabricante: VirtualBox
                   id físico: 1
                   información del bus: usb@1:1
                   versión: 1.00
                   capacidades: usb-1.10
                   configuración: driver=usbhid maxpower=100mA speed=12Mbit/s
        *-bridge
             descripción: Bridge
             producto: 82371AB/EB/MB PIIX4 ACPI
             fabricante: Intel Corporation
             id físico: 7
             información del bus: pci@0000:00:07.0
             versión: 08
             anchura: 32 bits
             reloj: 33MHz
             capacidades: bridge
             configuración: driver=piix4_smbus latency=0
             recursos: irq:9
        *-storage
             descripción: SATA controller
             producto: 82801HM/HEM (ICH8M/ICH8M-E) SATA Controller [AHCI mode]
             fabricante: Intel Corporation
             id físico: d
             información del bus: pci@0000:00:0d.0
             versión: 02
             anchura: 32 bits
             reloj: 33MHz
             capacidades: storage pm ahci_1.0 bus_master cap_list
             configuración: driver=ahci latency=64
             recursos: irq:21 ioport:d240(size=8) ioport:d248(size=4) ioport:d250(size=8) ioport:d258(size=4) ioport:d260(size=16) memoria:f0806000-f0807fff
     *-scsi:0
          id físico: 3
          nombre lógico: scsi1
          capacidades: emulated
        *-cdrom
             descripción: DVD reader
             id físico: 0.0.0
             información del bus: scsi@1:0.0.0
             nombre lógico: /dev/cdrom
             nombre lógico: /dev/dvd
             nombre lógico: /dev/sr0
             capacidades: audio dvd
             configuración: status=nodisc
     *-scsi:1
          id físico: 4
          nombre lógico: scsi2
          capacidades: emulated
        *-disk
             descripción: ATA Disk
             producto: VBOX HARDDISK
             id físico: 0.0.0
             información del bus: scsi@2:0.0.0
             nombre lógico: /dev/sda
             versión: 1.0
             serie: VBfa5c58a8-e01ba9e5
             tamaño: 10GiB (10GB)
             capacidades: partitioned partitioned:dos
             configuración: ansiversion=5 logicalsectorsize=512 sectorsize=512 signature=a455766f
           *-volume:0
                descripción: partición EXT4
                fabricante: Linux
                id físico: 1
                información del bus: scsi@2:0.0.0,1
                nombre lógico: /dev/sda1
                nombre lógico: /
                versión: 1.0
                serie: 01bcc737-9507-4181-9148-7b8e843d24eb
                tamaño: 9215MiB
                capacidad: 9215MiB
                capacidades: primary bootable journaled extended_attributes large_files huge_files dir_nlink extents ext4 ext2 initialized
                configuración: created=2018-12-09 00:17:42 filesystem=ext4 lastmountpoint=/ modified=2018-12-23 10:20:25 mount.fstype=ext4 mount.options=rw,relatime,errors=remount-ro,data=ordered mounted=2018-12-22 16:32:00 state=mounted
           *-volume:1
                descripción: Extended partition
                id físico: 2
                información del bus: scsi@2:0.0.0,2
                nombre lógico: /dev/sda2
                tamaño: 1022MiB
                capacidad: 1022MiB
                capacidades: primary extended partitioned partitioned:extended
              *-logicalvolume
                   descripción: Linux swap / Solaris partition
                   id físico: 5
                   nombre lógico: /dev/sda5
                   capacidad: 1022MiB
                   capacidades: nofs
st1*

## 2. Diseño e Implementación del Software

El software desarrollado se divide en 4 partes:

  -Lectura de parametros.
  -Configuracion y creacion de threads.
  -Operacion
  -Finalizacion.

- Lectura de parametros:
Lo que hace es leer los datos recibidos de argv[], pasados por la terminal y lo que hace es comprobar que los datos estan bien
introducidos. Si hay algo mal puesto se notifica y acaba el programa.

- Configuracion y creacion de threads:
 Aqui repartimos el trabajo para los threads, creamos o no el logger, y creamos los threads.

 ```c++
 std::thread(reduccion,trabajo+resto,&arrayDouble[0],&solucion,i);
```
A cada thread le pasamos el metodo (reduccion), el numero de iteraciones (trabajo + resto), la direccion de memoria donde empieza a iterar(&arrayDouble[0])
la direccion de memoria donde poner la solución(&solucion) y el numero de thread que es (i).

```c++
   std::thread(func_logger,nthreads,condFinal);
```
Al logger le pasamos su funcion (func_logger), el numero de threads creados (nthreads) y la variable condicional de sincronizacion con el main  (condFinal)

-Operacion:
Todos los threads realizan la operacion (sum o xor) sobre los numeros que se les han asignado.
Utilizo un mutex para la sincronizacion entre threads y otro para la sincronizacion logger-main.

-Finalizacion:
Aqui el main hace join a todos los threads, y obtiene los resultados calculados y lo compara con el obtenido por el logger (si hay logger).

## 3. Metodología y desarrollo de las pruebas realizadas

Descripción detallada del proceso de benchmarking, captura de resultados y generación de gráficas. Todo lo necesario para comenzar el análisis.

Algunos códigos de ejemplo:

**benchsuite.sh:**

```sh
#!/bin/bash
file=resultado.log #Nombre del archivo .log
data=data.dat #Nombre del arhivo .dat
rm resultado.log 2> /dev/null #Comentar si no quereis que se borre el fichero resultado cada vez que ejecuteis el script
make build > /dev/null #Compilamos la solución
gcc benchmarking/obtenMenor.c -o escribe
for benchcase in st1 mt1 st2 mt2; do #Realizamos las 4 operaciones diferentes
  echo $benchcase >> $file
  for i in `seq 1 1 50`; #Realizamos 10 iteraciones sobre el código con diferentes tamaños (mirar runner.sh)
  do
    for t in `seq 1 1 15`;
    do
      ./benchmarking/runner.sh $benchcase $i >> $file # results dumped
    done
    ./escribe
  done
  echo "" >> $data #Tenemos que dividir los datos de los diferentes operaciones por 2 \n
  echo "" >> $data
done
rm helper.dat
rm escribe
./benchmarking/creaGraficaTodo.sh
echo "Acaba el script, graficas creadas"
```
Los demas codigos estan en /benchmarking, puedes hecharles una ojeada

![](images/grafica.png)
![](images/graficaSpeedup.png)

## 4. Discusión

En este apartado se aportará una valoración personal de la práctica, en la que se destacarán aspectos como las dificultades encontradas en su realización y cómo se han resuelto.

También se responderá a las cuestiones formuladas si las hubiere.

1. Unas 18h mas o microsegundos
2. Solo me falta conectar con java
3. Utilizo un mutex para la sincronizacion entre threads-logger + variable_condicional, y una variable condicional entre logger-main.
 Si no hay logger, los hilos usan esa variable condicional para despertar al main.
4. Particiono como manda el enunciado. Se me ocurre igualar mas la carga de trabajo sumando el resto r a r threads, cada uno con una iteracion mas.


## 5. Propuestas optativas

justificar notify_all vs notify:

Utilizo notify_all, cuando quiero despertar a un hilo determinado, porque tengo la certeza de que lo va a despertar.

Veo mas razonable utilizar notifi_one si te da igual que hilo despertar, pero si hubisese uno que si al despertar no puedisese hacer nada,
el programa se quedaria en deadlock y no se podria hacer nada.

nombres variables:
Las variables tienen un nombre significativo.

Cambiar waits, get mutex:
Se accede con directivas de c++;

poner mutex al main, MAIN_FEATURE y arreglar operacion:
todo hecho, el fallo estaba en que no distinguia la operacion
...
