#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <thread>
#include <sys/time.h>
#include <condition_variable>

#define FEATURE_LOGGER 0
#define DEBUG 0
#define FEATURE_OPTIMIZE 1
#define ESCRITURA_DATOS 1
//variables sincronizacion logger-hilos

std::condition_variable condt;
std::mutex m; //tambien usado por el main

//variables logger--threads
double solucionaLogger=0; //variable global para pasar al logger los valores de los threads
int threadnum = 0; //variable global que dice al logger el numero del thread que pasa el valor por solucionaLogger
int vTotal = 0; //valor total para el main, cada hilo lo suma al acabar
bool ready = true; //variable que indica a los threads cuando el logger esta preparado para otro argumento

//variables logger--main
bool finLog = false; //varible que indica al main cuando ha acabado el logger

//variables threads--main
int threadsAcabados=0;

//variables logica del programa
int operacion = -1;
double *solLocalMain;
int nthreads =0;
//justificar notify_all vs notify(hecho)
//nombres variables (hecho)
//cambiar waits, get mutex (hecho)
//poner mutex al main (hecho)

double opera(double i1, double i2){
  if(operacion == 1){
    return i1 + i2;
  }else{
    return (int)i1 ^ (int) i2;
  }
}

void reduccion (int fin, double *array, double *retorno, int orden){

  double operacionLocal=0;

  if(operacion == 1){

    for(int i = 0; i<fin ; i++){
      (operacionLocal) += array[i];
    }

  }else{

    for(int i = 0; i<fin;i++){
      (operacionLocal) = (int)(operacionLocal) ^ (int) array[i];
    }

  }
  #if DEBUG
  puts("soy thread bloqueo ya");
  #endif

  std::unique_lock<std::mutex> lock(m);

  #if FEATURE_LOGGER
  condt.wait(lock,[]{return (ready);}); //wait
  solucionaLogger = operacionLocal; //resultado al logger
  #endif


  //Dar una vuelta al codigo-----------------------------------------------------------
  if(operacion ==1){

    (*retorno) += operacionLocal; //resultado al main

    #if FEATURE_LOGGER
    vTotal+= operacionLocal; //resultado al logger (global)
    #endif

  }else{

    *retorno = (int)(*retorno) ^ (int)(operacionLocal); //resultado al main

    #if FEATURE_LOGGER
    vTotal = (int)(vTotal) ^ (int)(operacionLocal); //resultado al logger (global)
    #endif
  }
  //--------------------------------------------------------------------------------------
  #if DEBUG
  printf("El resultado es: %lf \n",*retorno );
  #endif

  threadnum=orden;
  ready=false;
  threadsAcabados++;

  #if FEATURE_LOGGER
  condt.notify_all(); //cambio a notify all, necesario despertar al logger
  #endif

  #if !FEATURE_LOGGER
    if(threadsAcabados == nthreads)condt.notify_one();
  #endif
  lock.unlock();

  #if DEBUG
  printf("thread %d tiene: %lf \n",orden,*retorno);
  #endif
}


void func_logger (int nthreads, std::condition_variable *condi){

  int threadsProcesados;
  int orden[nthreads];
  double valoresPorThread[nthreads]; //guardamos la solucion de cada thread

  for(threadsProcesados=0; threadsProcesados<nthreads; threadsProcesados++){
    #if DEBUG
    puts("soy log bloqueo ya");
    #endif
    std::unique_lock<std::mutex> lock(m);

    #if DEBUG
    puts("soy log con mutex le suelto ahora");
    #endif

    condt.wait(lock,[]{condt.notify_one();
      return(!ready);}); //cambiar

      valoresPorThread[threadnum] = solucionaLogger;
      orden[threadsProcesados]=threadnum;
      //vTotal += solucionaLogger;
      ready= true;
      condt.notify_one();//cambio a notify
      lock.unlock();
    }
    #if DEBUG
    puts("Acabaron los hilos");
    #endif
    std::unique_lock<std::mutex> l(m);

    #if !FEATURE_OPTIMIZE
    vTotal =0; //si la optimizacion esta desactivada, calculamos aqui la suma de los resultados
    #endif

    puts("");
    printf("-------------------Orden:");
    for(int i=0;i<nthreads; i++){
      printf("%d ",orden[i]);
    }
    printf("-------------------\n");
    puts("");

    for(int i=0;i<nthreads; i++){
      printf("LOG: thread %d tiene: %lf \n",i,valoresPorThread[i]);

      #if !FEATURE_OPTIMIZE
      vTotal = opera(valoresPorThread[i],vTotal); //si no tiene optimizacion hay que calcular la suma
      #endif
    }

    finLog=true;
    (*condi).notify_one(); //cambio a notify
    l.unlock();
    puts("");

  }

  /*********************************************************************************************************************************/
  int main(int argc, char *argv[]){
    timeval t0;
    timeval t1;
    puts("");

    if(argc<3){
      printf("Parametros insuficientes\n");
      return 0;
    }

    int longitudArray = atoi(argv[1]);

    if(longitudArray <= 0){
      printf("Longitud invalida");
      return 0;

    }else{

      printf("Longitud del array %d\n", longitudArray);
    }


    if(strcmp(argv[2],"sum")== 0){
      operacion=1;

    }else if(strcmp(argv[2],"xor")==0){
      operacion=0;
    }
    if(operacion==-1){

      printf("Error en la suma/xor \n");
      return 0;
    }

    printf("Operacion: %s \n",argv[2]);

    nthreads= 0;

    if(argc < 5){
      nthreads =1;
    }else{
      nthreads = atoi(argv[4]);
      if(nthreads < 0 || nthreads > 10){
        printf("Error numero de hilos \n");
        return 0;
      }
    }

    printf("Num hilos: %d \n",nthreads);
    double  *arrayDouble;
    arrayDouble= (double *) (malloc(sizeof(double) * longitudArray));
    solLocalMain= (double *) (malloc(sizeof(double) * longitudArray));
    /*----------------------------------------------------------------------------------------------------------------------*/

    for(int i =0; i< longitudArray; i++){
      arrayDouble[i] =i;
    }

    std::thread threads[nthreads];

    int trabajo = (int)(longitudArray/nthreads); //trabajo haciendo la division exacta
    int resto = longitudArray % nthreads;
    printf("Trabajo por thread: %d, resto (1er thread): %d \n",trabajo,resto);

    //double solucionesThreads[nthreads]; version antigua
    double solucion=0;// ahora uso una variable donde se almacena el resultado final.
    std::thread log;
    std::condition_variable *condFinal;
    std::condition_variable fin;

    #if FEATURE_LOGGER
    condFinal = &fin;
    log= std::thread(func_logger,nthreads,condFinal);
    #else
    condFinal = &condt;
    #endif


    gettimeofday(&t0,NULL); //Inicia la operacion, luego inicio la cuenta

    threads[0] = std::thread(reduccion,trabajo+resto,&arrayDouble[0],&solucion,0);
    int trabajoThreadIni;
    for(int i=1;i<nthreads; i++){
      trabajoThreadIni =i*(trabajo)+resto;
      threads[i] = std::thread(reduccion,trabajo,&arrayDouble[trabajoThreadIni],&solucion,i);
    }

    //double sumTotal=0; ya no hace falta hacer calculos en el main

    std::unique_lock<std::mutex> l(m);

    #if FEATURE_LOGGER

    (*condFinal).wait(l,[] {return finLog;});

    #else

    (*condFinal).wait(l,[] {return nthreads == threadsAcabados;});

    #endif

    /**  (*condFinal).wait(l,[nthreads,condFinal] {(*condFinal).notify_one();
    return !((FEATURE_LOGGER==1 && !finLog) || (nthreads != threadsAcabados && FEATURE_LOGGER==0));}); //cambio el while**/


    #if FEATURE_LOGGER
    log.join();
    #endif

    for(int i=0;i<nthreads; i++){
      threads[i].join();
    }

    #if !FEATURE_OPTIMIZE

    for(int i=0;i<nthreads; i++){
      solucion = opera(solucion, solucionesThreads[i]);
    }

    #endif

    gettimeofday(&t1,NULL); //acaba la operacion, luego acabo la ejecucion
    puts("");

    #if FEATURE_LOGGER
    if(vTotal == solucion){
      printf("------------------EL VALOR COINCIDE---------------------------\n");
    }else{
      printf("------------------EL VALOR NO COINCIDE---------------------------\n");
      return 1;
    }

  #endif

  puts("");
  printf("LA REDUCCION TIENE EL VALOR DE: %lf \n",solucion);

  long transcurrido = (t1.tv_sec-t0.tv_sec)*1000000+ (t1.tv_usec-t0.tv_usec);
  printf("Tiempo = %ld microsegundos \n", transcurrido);
  puts("");


  #if ESCRITURA_DATOS
  FILE *file = fopen("helper.dat", "a"); //"a" para hacer append
  if (file == NULL)
  {
    printf("Error opening file!\n");
    exit(1);
  }
  fprintf(file,"%d %ld\n",longitudArray,transcurrido); //Tamaño en las X, SpeedUp en las Y
  fclose(file);
  #endif


  return 0;

}
