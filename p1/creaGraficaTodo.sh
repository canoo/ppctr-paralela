#!/bin/bash
gnuplot <<EOF
set term png size 1000,1000
set output "images/grafica.png"
set style line 1 linecolor rgb '#ff0000' linetype 1 linewidth 2 pointtype 7 pointsize 1.5
set style line 2 linecolor rgb '#00ff00' linetype 1 linewidth 2 pointtype 7 pointsize 1.5
set style line 3 linecolor rgb '#0000ff' linetype 1 linewidth 2 pointtype 7 pointsize 1.5
set style line 4 linecolor rgb '#ff00ff' linetype 1 linewidth 2 pointtype 7 pointsize 1.5
set title "Grafica $season P1"
set key right center
set ylabel 'Time' offset 2,1 rotate by 0
set xlabel 'Size' offset 2,1 rotate by 0
plot 'data.dat' index 0 title 'SingleThread XOR' with linespoints linestyle 1,'' index 1 title 'MultiThread XOR' with linespoints linestyle 2,'' index 2 title 'SingleThread SUM' with linespoints linestyle 3,'' index 3 title 'MultiThread SUM' with linespoints linestyle 4
EOF

gcc benchmarking/speedUP.c -o speedUP
./speedUP

gnuplot <<EOF
set term png size 1000,1000
set output "images/graficaSpeedup.png"
set style line 1 linecolor rgb '#ff0000' linetype 1 linewidth 2 pointtype 7 pointsize 1.5
set style line 2 linecolor rgb '#00ff00' linetype 1 linewidth 2 pointtype 7 pointsize 1.5
set title "Grafica $season P1"
set key right center
set ylabel 'Speedup' offset 2,1 rotate by 0
set xlabel 'Size' offset 2,1 rotate by 0
plot 'speed.dat' index 0 title 'Speedup XOR' with linespoints linestyle 1,'' index 1 title 'Speedup SUM' with linespoints linestyle 2
EOF
rm speedUP
rm data.dat
rm speed.dat
echo -ne "\033[0K\r"
