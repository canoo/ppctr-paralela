#!/bin/bash
rm *.dat
file=resultado.log #Nombre del archivo .log
data=data.dat #Nombre del arhivo .dat
rm resultado.log 2> /dev/null #Comentar si no quereis que se borre el fichero resultado cada vez que ejecuteis el script
make build > /dev/null #Compilamos la solución
gcc benchmarking/obtenMenor.c -o escribe
for benchcase in st1 mt1 st2 mt2; do #Realizamos las 4 operaciones diferentes
  echo $benchcase >> $file
  for i in `seq 1 1 50`; #Realizamos 50 iteraciones sobre el código con diferentes tamaños
  do
    for t in `seq 1 1 15`; #ejecutamos 15 veces y nos quedamps con el mejor tiempo
    do
      ./benchmarking/runner.sh $benchcase $i >> $file # results dumped
    done
    ./escribe
  done
  echo "" >> $data #Tenemos que dividir los datos de los diferentes operaciones por 2 \n
  echo "" >> $data
done
rm helper.dat
rm escribe
./benchmarking/creaGraficaTodo.sh
echo "Acaba el script, graficas creadas"
