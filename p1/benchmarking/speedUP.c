#include <stdio.h>

int main(){

  FILE *file = fopen("data.dat", "r");
  if (file == NULL)
  {
    printf("Error opening file!\n");
    return 0;
  }

  double sumPar[50];
  double xorPar[50];
  double sumSec[50];
  double xorSec[50];
  double speedupSum[50];
  double speedupXor[50];
  double data[50];

  for(int i=0; i<50; i++){
    fscanf(file,"%lf %lf",&data[i],&xorSec[i]);
  }

  for(int i=0; i<50; i++){
    fscanf(file,"%lf %lf",&data[i],&xorPar[i]);
    speedupXor[i]= xorSec[i]/xorPar[i];
    //printf("%lf\n", speedupXor[i] );
  }

  for(int i=0; i<50; i++){
    fscanf(file,"%lf %lf",&data[i],&sumSec[i]);

  }
  puts("");

  for(int i=0; i<50; i++){
    fscanf(file,"%lf %lf",&data[i],&sumPar[i]);
    speedupSum[i]= sumSec[i]/sumPar[i];
    //printf("%lf\n", speedupSum[i] );
  }
  fclose(file);


  file = fopen("speed.dat", "w");

  for(int i =0; i<50; i++){
    fprintf(file, "%lf %lf \n",data[i],speedupXor[i] );
  }

  fprintf(file, "\n \n");

  for(int i =0; i<50; i++){
    fprintf(file, "%lf %lf\n",data[i],speedupSum[i]);
  }

  fclose(file);

  return 0;
}
