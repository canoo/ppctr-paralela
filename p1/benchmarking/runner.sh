#!/usr/bin/env bash
size=$((112000 * $2)) #Tamaño minimo 512000 Tamaño maximo 5120000
nthreads=$((4)) #Usara el numero maximo de threads del ordenador. Modificar si se gusta.
case "$1" in
    st1)
        ./build/p1 $size xor
        ;;
    mt1)
        ./build/p1 $size xor --multi-thread $nthreads
        ;;
    st2)
        ./build/p1 $size sum
        ;;
    mt2)
        ./build/p1 $size sum --multi-thread $nthreads
        ;;
esac
