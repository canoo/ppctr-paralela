#include <stdio.h>


int main(){

  FILE *file = fopen("helper.dat", "r");
  if (file == NULL)
  {
    printf("Error opening file!\n");
    return 0;
  }
  double d;
  double das;

  fscanf(file,"%lf %lf",&das,&d);
  double sig;

  for(int i=1; i<15; i++){
    fscanf(file,"%lf %lf",&das,&sig);
    if(sig<d) d= sig;
  }
  fclose(file);
  fclose(fopen("helper.dat", "w"));

  file = fopen("data.dat", "a");
  if (file == NULL)
  {
    printf("Error opening file!\n");
    return 0;
  }
  fprintf(file, "%lf %lf\n",das,d);
  fclose(file);
  return 0;
}
