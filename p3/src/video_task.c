#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>

double tiempo;
double tiempofin;
void fgauss (int *, int *, int, int);

int main(int argc, char *argv[]) {

  FILE *in;
  FILE *out;
  int i, j, size=1, seq = 8;
  int *pixels, *filtered;
  int paralelo = 4;



  if (argc == 2) seq = atoi (argv[1]);

  //   chdir("/tmp");
  in = fopen("movie.in", "rb");
  if (in == NULL) {
    perror("movie.in");
    exit(EXIT_FAILURE);
  }

  out = fopen("movie.out", "wb");
  if (out == NULL) {
    perror("movie.out");
    exit(EXIT_FAILURE);
  }

  int width, height;

  fread(&width, sizeof(width), 1, in);
  fread(&height, sizeof(height), 1, in);

  fwrite(&width, sizeof(width), 1, out);
  fwrite(&height, sizeof(height), 1, out);

  filtered = (int *) malloc(paralelo *(height+2) * (width+2) * sizeof(int));
  pixels = (int *) malloc(paralelo *(height+2) * (width+2) * sizeof(int));


  #pragma omp parallel num_threads(paralelo) shared(width,height)
  {
    #pragma omp single
    {
      tiempo = omp_get_wtime();

      while(size){
        size = fread(pixels, (height+2) * (width+2) * sizeof(int), paralelo, in);

        for(int i =0; i<size; i++){

          #pragma omp task shared (pixels,filtered) firstprivate(i)
          {
            fgauss (&pixels[i*(height+2) * (width+2)], &filtered[i*(height+2) * (width+2)], height, width);
          }
        }

        #pragma omp taskwait
        fwrite(filtered, (height+2) * (width + 2) * sizeof(int), size, out);
      }
    }
  }

  tiempofin = omp_get_wtime();

  free(pixels);
  free(filtered);

  fclose(out);
  fclose(in);
  //printf("Tiempo de ejecucion: %lf\n",tiempofin - tiempo );

  FILE *f = fopen("dataParalelo.dat", "a");
  if (f == NULL)
  {
    printf("Error opening file!\n");
    exit(1);
  }
  fprintf(f,"%lf\n",tiempofin - tiempo);
  fclose(f);

  return 0;
}

void fgauss (int *pixels, int *filtered, int heigh, int width)
{
  int y, x, dx, dy;
  int filter[5][5] = {1, 4, 6, 4, 1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4, 1, 4, 6, 4, 1};
  int sum;

  for (x = 0; x < width; x++) {
    for (y = 0; y < heigh; y++)
    {
      sum = 0;
      for (dx = 0; dx < 5; dx++)
      for (dy = 0; dy < 5; dy++)
      if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < heigh))
      sum += pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];
      filtered[x*heigh+y] = (int) sum/273;
    }
  }
}
