#include <stdio.h>

int main(){

  double valorPar;
  double valorSec;
  double aux;

   FILE *f = fopen("dataParalelo.dat", "r");
  if (f == NULL)
  {
    printf("Error opening file!\n");
    return 0;
  }

  fscanf (f, "%lf", &valorPar);
  while (!feof(f)) {
    fscanf (f, "%lf", &aux);
    if(aux < valorPar){
      valorPar = aux;
    }
 }

 fclose(f);

   f = fopen("dataSecuencial.dat", "r");
  if (f == NULL)
  {
    printf("Error opening file!\n");
      return 0;
  }

  fscanf(f,"%lf",&valorSec);
  while (!feof(f)) {
    fscanf (f, "%lf", &aux);
    if(aux<valorSec){
      valorSec = aux;
    }
 }

  fclose(f);

  printf("Mejor tiempo paralelo: %lf \n",valorPar);
  printf("Mejor tiempo secuencial: %lf\n",valorSec);
  printf("Speedup: %lf\n",valorSec/valorPar);

    return 0;

}
