# P3: VideoTask

1. En el programa secuencial, no entiendo porque se reserva memoria para una secuencia entera,(en las variables pixels y filtered), cuando nunca se llega a usar
porque lo unico que hace es leer, fgauss y escribir en fichero, luego no es necesario reservar tanta memoria, lo optimo seria reservar solo para un bloque de (height+2) * (width+2) * sizeof(int) para el secueincial.
Por lo tanto ```**pixels``` pasaria a ser ```*pixels```, al igual que filtered. Yo despues, como queria hacer tratamiento paralelo, he tenido que aumentar la memoria a num_threads* (height+2) * (width+2) * sizeof(int)
y guardar estos trozos de memoria en ```*pixels``` y ```*filtered```.

2.
#pragma omp task:
Lo que hace task es crear "tareas" las cuales van siendo cogidas por los threads disponibles.

#pragma omp taskwait:
El hilo que llega aqui, espera a que acaben todas las tasks creadas.

3.
shared (pixels,filtered,width,height) firstprivate(i)

pixels y filtered son vectores de datos, y cada task, va a acceder y modificar una parte independiente a la de los demas,
por lo que es shared y no requiere sincronizacion.
width y height son "finales" y solo nos sirven como lectura
la i se va a utilizar en cada task de forma privada, con el valor que se creo esta, para acceder a su bloque de memoria.

4.
Este programa lo paralelizo leyendo mas datos que el anterior de manera secuencial y tratandolos individualmente de manera paralela. Despues hago una espera a que acaben todas las tasks de tratar su parte, para escribir de manera ordenada y secuencial los datos en el fichero, y al acabar volvemos a iterar. Asi hasta acabar.


5.
He conseguido tener un tiempo en paralelo de 0.781286, contra el tiempo secuencial inicial que era de  2.408807. Esto da un speedup de 3, por lo que es bastante eficiente la paralelizacion.
Esta ganancia es debido a que mi ordenador tiene 4 cores, luego es capaz de hacer 4 operaciones simultaneamente. si ejecutas la macro obtenData.sh ejecuta los programas muchas veces y luego obtiene
el menor tiempo obtenido de cada uno y calcula el speedup y lo muestra por la terminal.

6.
No encuentro ninguna optmizacion secuencial, y sobre el programa paralelizado no estaria bien volver a paralelizar ( a no ser que tu maquina tenga muchos cores ). Si hubiese que aplicar una optimizacion sobre
la funcion en el programa secuencial, se podria hacer una region paralela y un #pragma omp for, ya que las iteraciones del primer bucle son independientes.
